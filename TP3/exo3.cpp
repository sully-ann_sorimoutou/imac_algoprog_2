#include "mainwindow.h"
#include "tp3.h"
#include <QApplication>
#include <time.h>
#include <stack>
#include <queue>

MainWindow *w = nullptr;
using std::size_t;

struct BinarySearchTree : public BinaryTree
{
    Node *left;
    Node *right;
    int value;

    void initNode(int valueOne)
    {
        this->left = NULL;
        this->right = NULL;
        this->value = valueOne;
        // init initial node without children
    }

    void insertNumber(int value)
    {
        if (value > this->value)
        {
            if (this->right == NULL)
            {
                this->right = createNode(value);
            } else {
                this->right->insertNumber(value);
            }
        } else {
            if(this->left == NULL)
            {
                this->left = createNode(value);
            } else{
                this->left->insertNumber(value);
            }
        }
        // create a new node and insert it in right or left child
    }

    uint height() const
    {   
    int hauteurD=0;
    int hauteurG=0;

        if(this->right==NULL && this->left==NULL){
            return 1;
        } else{
            if(this->right!=NULL){
                hauteurD = this->right->height();
            }
            if(this->left!=NULL){
                hauteurG = this->left->height();
            }
            
            if(hauteurD>hauteurG){
                return hauteurD++;
            }else{
                return hauteurG++;
            }
        }
        // should return the maximum height between left child and
        // right child +1 for itself. If there is no child, return
        // just 1
        
    }

    uint nodesCount() const
    {
    int sum=0;

        if(this->right==NULL && this->left==NULL){
            return 1;
        } else {
            if(this->right!=NULL){
                sum = this->right->nodesCount(); 
            }
            if(this->left!=NULL){
                sum += this->left->nodesCount();
            }
            return sum ++;
        }
        // should return the sum of nodes within left child and
        // right child +1 for itself. If there is no child, return
        // just 1
    }

    bool isLeaf() const
    {   
        if(this->right==NULL && this->left==NULL){
            return true;
        }else{
            return false;
        }
        // return True if the node is a leaf (it has no children)

    }

    void allLeaves(Node *leaves[], uint &leavesCount)
    {
        if(this->isLeaf()){
            leaves[leavesCount]=this;
            leavesCount++;
        } else{
            if(this->left!=NULL){
                this->left->allLeaves(leaves, leavesCount);
            }
            if(this->right!=NULL){
                this->right->allLeaves(leaves,leavesCount);
            }
        }
        // fill leaves array with all leaves of this tree
    }

    void inorderTravel(Node *nodes[], uint &nodesCount)
    {
        if(this->left!=NULL){
            this->left->inorderTravel(nodes, nodesCount);
        }
        nodes[nodesCount]=this;
        nodesCount ++;

        if(this->right!=NULL){
            this->right->inorderTravel(nodes, nodesCount);
        }

        // fill nodes array with all nodes with inorder travel
    }

    void preorderTravel(Node *nodes[], uint &nodesCount)
    {
        nodes[nodesCount]=this;
        nodesCount ++;

        if(this->left!=NULL){
            this->left->preorderTravel(nodes, nodesCount);
        }

        if(this->right!=NULL){
            this->right->preorderTravel(nodes, nodesCount);
        }

        
        
        // fill nodes array with all nodes with preorder travel
    }

    void postorderTravel(Node *nodes[], uint &nodesCount)
    {
        if(this->left!=NULL){
            this->left->postorderTravel(nodes, nodesCount);
        }

        if(this->right!=NULL){
            this->right->postorderTravel(nodes, nodesCount);
        }

        nodes[nodesCount]=this;
        nodesCount ++;

        // fill nodes array with all nodes with postorder travel
    }

    Node *find(int value)
    {
        if(this->isLeaf()&& this->value!=value){
            return NULL
        } else {
            if(this->value==value){
                return this;
            }else{
                if(this->value < value){
                    if(this->right!=NULL){
                        this->right->find(value);
                    }else{
                        return NULL;
                    }
                    
                }else{
                   if(this->left!=NULL){
                        this->left->find(value);
                    }else{
                        return NULL;
                    }  

                }
          
            }
        }
        // find the node containing value
        return nullptr;
    }

    void reset()
    {
        if (left != NULL)
            delete left;
        if (right != NULL)
            delete right;
        left = right = NULL;
    }

    BinarySearchTree(int value) : BinaryTree(value) { initNode(value); }
    ~BinarySearchTree() {}
    int get_value() const { return value; }
    Node *get_left_child() const { return left; }
    Node *get_right_child() const { return right; }
};

Node *createNode(int value)
{
    return new BinarySearchTree(value);
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow::instruction_duration = 200;
    w = new BinarySearchTreeWindow<BinarySearchTree>();
    w->show();

    return a.exec();
}
