#include <QApplication>
#include <time.h>
#include "tp2.h"

MainWindow *w = nullptr;

void merge(Array &first, Array &second, Array &origin);

void splitAndMerge(Array &origin)
{
	// stop statement = condition + return (return stop the function even if it does not return anything)
	if (origin.size() <= 1)
	{
		return;
	}

	// initialisation
	Array &first = w->newArray(origin.size() / 2);
	Array &second = w->newArray(origin.size() - first.size());

	// split
	for (int i = 0; i < first.size(); i++)
	{
		first[i] = origin[i];
	}

	for (int i = 0; i < second.size(); i++)
	{
		second[i] = origin[i + first.size()];
	}
	// recursiv splitAndMerge of lowerArray and greaterArray

	splitAndMerge(first);
	splitAndMerge(second);
	// merge
	merge(first, second, origin);
}

void merge(Array &first, Array &second, Array &origin)
{
	int indiceFirst = 0;
	int indiceSecond = 0;

	for (int i = 0; i < first.size() + second.size(); i++)
	{

		if (indiceFirst == first.size())
		{
			origin[i] = second[indiceSecond];
			indiceSecond++;
		}

		else if (indiceSecond == second.size())
		{
			origin[i] = first[indiceFirst];
			indiceFirst++;
		}

		else if (first[indiceFirst] < second[indiceSecond])
		{
			origin[i] = first[indiceFirst];
			indiceFirst++;
		}
		else if (second[indiceSecond] <= first[indiceFirst])
		{
			origin[i] = second[indiceSecond];
			indiceSecond++;
		}
	}
}

void mergeSort(Array &toSort)
{
	splitAndMerge(toSort);
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 50;
	w = new TestMainWindow(mergeSort);
	w->show();

	return a.exec();
}
