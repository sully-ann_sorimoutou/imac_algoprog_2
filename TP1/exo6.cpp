#include <iostream>

using namespace std;

struct Noeud
{
    int donnee;
    Noeud *suivant;
};

struct Liste
{
    Noeud *premier;
    int nombreNoeud;
    Noeud *dernierNoeud;
    // your code
};

struct DynaTableau
{
    int *donnees;
    int taille;
    int capacite;
};

// initialise le curseur liste et le premier noeud
void initialise_liste(Liste *liste)
{

    liste->premier = NULL;
    liste->nombreNoeud = 0;
    liste->dernierNoeud = NULL;
}

bool est_vide_liste(const Liste *liste)
{
    if (liste->nombreNoeud == NULL)
    {
        return true;
    }
    return false;
}

void ajoute_liste(Liste *liste, int valeur)
{

    // création d'un nouvel élément
    Noeud *nouveauNoeud = new Noeud;
    if (nouveauNoeud == NULL)
    {
        cout << "Allocation echec" << endl;
        exit(1);
    }

    // insertion élément au début

    /* nouveauNoeud->donnee=valeur;
    noeud->suivant=liste->premier;
    liste->premier=nouveauNoeud;*/

    if (liste->premier == NULL)
    {
        liste->premier = nouveauNoeud;
        liste->dernierNoeud = nouveauNoeud;
    }
    else
    {
        liste->dernierNoeud->suivant = nouveauNoeud;
        liste->dernierNoeud = nouveauNoeud;
    }

    // noueud +1
    liste->nombreNoeud = liste->nombrebNoeud++;
}

void affiche_liste(const Liste *liste)
{
    Noeud *noeudActuel = liste->premier;
    while (noeudActuel->suivant != NULL)
    {
        cout << noeudActuel->donnee << endl;
        noeudActuel = noeudActuel->suivant;
    }
    cout << endl;
}

int recupere_liste(const Liste *liste, int n)
{
    Noeud *noeudActuel = liste->premier;
    int i = 1;
    while ((noeudActuel->suivant != NULL) && (i <= n))
    {
        noeudActuel = noeudActuel->suivant;
        i++;
    }
    return noeudActuel->donnee;
}

int cherche(const Liste *liste, int valeur)
{

    Noeud *noeudActuel = liste->premier;
    int i = 1;
    while (noeudActuel != NULL)
    {
        if (noeudActuel->donnee == valeur)
        {
            return i;
        }
        i++;
        noeudActuel = noeudActuel->suivant;
    }
    return -1;
}

void stocke(Liste *liste, int n, int valeur)
{
    Noeud *nouveuNoeud = new Noeud;
    nouveauNoeud->donnee = valeur;
    Noeud *noeudActuel = liste->premier;
    int i = 1;
    while (noeudActuel != NULL && i < n - 1)
    {
        i++;
        noeudActuel = noeudActuel->suivant;
    }
    nouveauNoeud->suivant = noeudActuel->suivant;
    noeudActuel->suivant = nouveauNoeud;
}

void ajoute(DynaTableau *tableau, int valeur)
{
    if (tableau->capacite == tableau->taille)
    {
        tableau->capacite += 5;
    }
    tableau->donnees = (int *)realloc(tableau->donnees, (tableau->taille + 1) * sizeof(int));
    if (tableau->donnees == NULL)
    {
        cout << "Allocation a échouée" << endl;
        exit(1);
    }
    tableau->donnees[tableau->taille] = valeur;
    tableau->taille++;
}

void initialise(DynaTableau *tableau, int capacite)
{
    tableau->donnees = (int *)malloc(sizeof(int));
    tableau->taille = 0;
    tableau->capacite = capacite;
    if (tableau->donnees == NULL)
    {
        cout << "Allocation a échouée " << endl;
        exit(1);
    }
}

// pour les tableaux 

bool est_vide(const DynaTableau *liste)
{
    if (liste == NULL)
    {
        return true;
    }
    return false;
}

void affiche(const DynaTableau *tableau)
{
    for (int i = 0; i < tableau->taille; i++)
    {
        cout << tableau->donnees[i] << endl;
    }
    cout << endl;
}

int recupere(const DynaTableau *tableau, int n)
{
    if (tableau->taille < n or n == 0)
    {
        return 0;
    }
    else
    {
        return tableau->donnees[n];
    }
}
int cherche(const DynaTableau *tableau, int valeur)
{
    for (int i = 0; i < tableau->taille; i++)
    {
        if (tableau->donnees[i] == valeur)
        {
            return i + 1;
        }
    }
    return -1;
}

void stocke(DynaTableau *tableau, int n, int valeur)
{
    if (tableau->taille < n or n == 0)
    {
        cout << "peu pas stocker" << endl;
    }
    else
    {
        tableau->donnees[n - 1] = valeur;
    }
}

void pousse_file(Liste* liste, int valeur)
{
    ajoute(liste, valeur);
}

int retire_file(Liste* liste)
{
    Noeud * noeudActuel = liste->premier;
    int val = nouveauActuel->donnee;

    liste->premier = liste->premier->suivant;
    liste->nombreNoeud = liste->nombreNoeud - 1;
    free(tmp);

    return val;
}

void pousse_pile(Liste* liste, int valeur)
{
  Noeud * nouveauNoeud = new Noeud;

    nouveauNoeud->donnee = valeur;
    nouNoveaueud->suivant = liste->premier;
    liste->premier = nouveauNoeud;
    liste->nombreNoeud = liste->nombreNoeud + 1;
}

int retire_pile(Liste* liste)
{
    Noeud * noeudActuel = liste->premier;
    int valeur = noeudActuel->donnee;

    liste->premier = liste->premier->suivant;
    liste->nombreNoeud = liste->nombreNoeud - 1;
    free(noeudActuel);

    return valeur;
   
}

int main()
{
    Liste liste;
    initialise_liste(&liste);
    DynaTableau tableau;
    initialise(&tableau, 5);

    if (!est_vide_liste(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (!est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    for (int i = 1; i <= 7; i++)
    {
        ajoute_liste(&liste, i * 7);
        ajoute(&tableau, i * 5);
    }

    if (est_vide_liste(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    std::cout << "Elements initiaux:" << std::endl;
    affiche_liste(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    std::cout << "5e valeur de la liste " << recupere_liste(&liste, 4) << std::endl;
    std::cout << "5e valeur du tableau " << recupere(&tableau, 4) << std::endl;

    std::cout << "21 se trouve dans la liste à " << cherche_liste(&liste, 21) << std::endl;
    std::cout << "15 se trouve dans la liste à " << cherche(&tableau, 15) << std::endl;

    stocke_liste(&liste, 4, 7);
    stocke(&tableau, 4, 7);

    std::cout << "Elements après stockage de 7:" << std::endl;
    affiche_liste(&liste);
    affiche(&tableau);
    std::cout << std::endl;
}
