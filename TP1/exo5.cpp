#include "tp1.h"
#include <QApplication>
#include <time.h>
#include <math.h>

void carre(Point *z){
    float a = z->x; 
    float b = z->y;
    z->x = a*a - b*b;
    z->y = 2*a*b;
}

int isMandelbrot(Point z, int n, Point point){
    // recursiv Mandelbrot
    carre(&z);
    z.x = z.x + point.x;
    z.y = z.y + point.y;

  
    float module = sqrt(z.x* z.x + z.y * z.y);

        if(module >2){
            return 0;
        }
        if(n==0){
            return 1;
        }
        
        else{
            return isMandelbrot(z, n-1, point);
            
        }

}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow* w = new MandelbrotWindow(isMandelbrot);
    w->show();

    a.exec();
}



